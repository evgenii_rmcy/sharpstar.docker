# SETUP
1. docker-compose build
2. docker-compose up -d
3. cp ~/.ssh ./volumes/launchpad/.ssh
4. docker exec -it sharpstar_launchpad bash
   1. chmod 400 /root/.ssh/id_rsa
   2. cd /opt/sharpstar
   3. compose install
   4. cd /root/.certs
   5. mkcert landingpages.localhost 
5. cp <postgres_dump> ./volumes/postgres/dumps
6. docker exec -it sharpstar_postgres bash
   1. psql -U <user> -d <database> -c 'CREATE SCHEMA campaign_leads;'
   2. pg_restore -U <user> -d <database> --role=<user> --verbose --clean --no-acl --no-owner <dump_filepath>
   3. Also, you can drop tables and relations
      1. psql -U sharpstar -d sharpstar -c "DROP SCHEMA public CASCADE;"
      2. psql -U sharpstar -d sharpstar -c "DROP SCHEMA campaign_leads CASCADE;"
      3. psql -U sharpstar -d sharpstar -c "CREATE SCHEMA public;"
      4. psql -U sharpstar -d sharpstar -c "CREATE SCHEMA campaign_leads"
7. docker-compose stop && docker-compose up -d
8. chrome://flags/#allow-insecure-localhost